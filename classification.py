import pandas as pd
from verispy import VERIS

# get pandas from json
data_dir = '../VCDB/data/json/validated/'
v = VERIS(json_dir=data_dir)
veris_df = v.json_to_df(verbose=False)

df = veris_df.dropna(axis=1, thresh=6000)
df = df.drop(columns=df.columns[(df == False).all()])

non_bool = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64',
            'object']
non_bool_df = df.select_dtypes(include=non_bool)

# for csv features
non_bool_df = non_bool_df.iloc[:, 11:]

# boolean part of dataframe
bool_df = df.select_dtypes(include=bool)
print(bool_df.head())

# inverse one hot encoding pre
df = pd.DataFrame()
for _, row in bool_df.iterrows():
    dic = {}
    for key in row.keys():
        if row[key]:
            if len(key.split('.')) == 2 and\
                    'action' in key.split('.'):
                dic[key.split('.')[0]] = key.split('.')[-1]

            if len(key.split('.')) == 3 and\
                    'victim' in key.split('.') and\
                    'employee_count' in key.split('.'):
                dic['victim.' + key.split('.')[1]] = key.split('.')[-1]

            if len(key.split('.')) == 3 and\
                    'victim' in key.split('.') and\
                    'country' in key.split('.'):
                dic['victim.' + key.split('.')[1]] = key.split('.')[-1]

            if len(key.split('.')) == 2 and\
                    'actor' in key.split('.'):
                dic[key.split('.')[0]] = key.split('.')[-1]

            if len(key.split('.')) == 4 and\
                    'asset' in key.split('.') and\
                    'variety' in key.split('.'):
                dic[key.split('.')[0]] = key.split('.')[-1].split()[0]

            if len(key.split('.')) == 4 and\
                    'attribute' in key.split('.') and\
                    'data_disclosure' in key.split('.'):
                dic[key.split('.')[-2]] = key.split('.')[-1]

    df = df.append(pd.Series(dic), ignore_index=True)
    print(df)
