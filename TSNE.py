# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 17:58:49 2020

@author: spele
"""
%matplotlib inline

vmat = v.df_to_matrix(vcdb)

from verispy import VERIS
import pandas as pd
data_dir = '../VCDB/data/json/validated/'
v = VERIS(json_dir=data_dir)
vcdb = v.json_to_df(verbose=False)
vcdb.shape

from sklearn.manifold import TSNE
tsne = TSNE(n_components=2, random_state=42)
v_tsne = tsne.fit_transform(vmat)

import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

tsne_df = pd.DataFrame({'x':v_tsne[:, 0], 'y':v_tsne[:, 1], 'pattern':patterns['pattern']})
tsne_df.head()

tsne_centers = tsne_df.groupby(by='pattern').mean()
tsne_centers['pattern'] = tsne_centers.index

p1 = sns.lmplot(x='x', y='y', data=tsne_df, fit_reg=False, hue='pattern',
                scatter_kws={'alpha':0.25}, size=6)

p1.savefig("tsne2.png")

def label_point(df, ax):
    for i, point in df.iterrows():
        ax.text(point['x'] - 30, point['y'], point['pattern'])

label_point(tsne_centers, plt.gca())

plt.show()