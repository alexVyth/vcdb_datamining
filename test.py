from verispy import VERIS
data_dir = '../VCDB/data/json/validated/'
v = VERIS(json_dir=data_dir)
veris_df = v.json_to_df(keep_raw=True, verbose=False)
veris_raw = v.raw_df
