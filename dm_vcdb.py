#!/usr/bin/env python
# coding: utf-8

# In[33]:


get_ipython().run_line_magic('matplotlib', 'inline')
from verispy import VERIS

data_dir = '../VCDB/data/json/validated/'
v = VERIS(json_dir=data_dir)
veris_df = v.json_to_df(verbose=False)
veris_df.shape














# In[35]:


print(veris_df['action.malware.variety.Ransomware'].value_counts())
print(veris_df['action.malware.variety.Ransomware'].value_counts())


# In[36]:


import pprint
pprint.pprint(v.enumerations)


# In[37]:


v.enum_summary(veris_df, 'action')


# In[38]:


v.enum_summary(veris_df, 'action', by='attribute')


# In[39]:


v.enum_summary(veris_df, 'action.social.variety', ci_method='normal', ci_level=0.95)


# In[40]:


action_fig = v.plot_barchart(v.enum_summary(veris_df, 'action'), 'Actions')


# In[45]:


from plotnine import *
ggplot(v.enum_summary(veris_df, 'action')) + aes(x='freq') + geom_bar(size=1, alpha=0.8) # defining the type of plot to use


# In[42]:


from dbir_patterns import get_pattern
patterns = get_pattern(veris_df)
patterns['pattern'].value_counts()


# In[12]:


vmat = v.df_to_matrix(veris_df)
vmat


# In[23]:


from sklearn.manifold import TSNE
tsne = TSNE(n_components=2, random_state=42)
v_tsne = tsne.fit_transform(vmat)


# In[24]:


import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

tsne_df = pd.DataFrame({'x':v_tsne[:, 0], 'y':v_tsne[:, 1], 'pattern':patterns['pattern']})
tsne_df.head()


# In[25]:


tsne_centers = tsne_df.groupby(by='pattern').mean()
tsne_centers['pattern'] = tsne_centers.index


# In[26]:


p1 = sns.lmplot(x='x', y='y', data=tsne_df, fit_reg=False, hue='pattern',
                scatter_kws={'alpha':0.25}, size=6)


# In[22]:


def label_point(df, ax):
    for i, point in df.iterrows():
        ax.text(point['x'] - 30, point['y'], point['pattern'])


# In[56]:


label_point(tsne_centers, plt.gca())


# In[60]:


plt.show()


# In[59]:





# In[ ]:




