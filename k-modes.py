# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 18:05:08 2020

@author: spele
"""

import numpy as np
from kmodes.kmodes import KModes
from   kmodes import kprototypes

# random categorical data
df = pd.read_csv("final.csv")

#filter out sparse numerical vars
df = df[['action', 'actor', 'asset.assets.variety',
       'asset.variety', 'attribute',
       'attribute.confidentiality.data_disclosure',
       'attribute.confidentiality.data.variety',
       'pattern', 'victim.country',
       'victim.industry.name', 'victim.orgsize']]

data = df.values
ls =[]
centers = []
for i in range(1,20,1):
    print(i)
    km = KModes(n_clusters=i, init='Huang', n_init=8, verbose=1)
    clusters = km.fit_predict(data)
    ls.append(km.cost_)
    centers.append(km.cluster_centroids_)

import matplotlib.pyplot as plt
plt.figure()
plt.plot(ls)
plt.xlabel("Number of clusters")
plt.ylabel("Dissimilarity")
plt.show()

a = np.argmin(np.asarray(ls))
print("Number of clusters", a+1)
best_centers = centers[a]
# Print the cluster centroids
print("Cluster Centers:", best_centers)