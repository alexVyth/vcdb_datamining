# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 15:02:56 2020

@author: spele
"""
""" Here we only process the boolean part of the dataset """
""" Confirms the dependent nature of our dataset. Parents and children
appear together"""

# =============================================================================
# Part 1 (All dataset -> Hierarchy)
# =============================================================================
import pandas as pd
bool_df = pd.read_csv("bool.csv")
from mlxtend.frequent_patterns import association_rules, fpgrowth
# Building the model 
frq_items = fpgrowth(bool_df, min_support = 0.15, use_colnames = True) 
# Collecting the inferred rules in a dataframe 
rules = association_rules(frq_items, metric ="lift", min_threshold = 1) 
rules = rules.sort_values(['confidence', 'lift'], ascending =[False, False]) 

# =============================================================================
# Part 2: Discover usual suspects between countries
# =============================================================================

enemies = pd.read_csv("bool.csv")
ext = [col for col in enemies.columns if 'actor.external.country.' in col]
vict = [col for col in enemies.columns if 'victim.country.' in col]
enemies =  enemies[ext+vict]

# Building the model 
frq_items = fpgrowth(enemies, min_support = 0.002, use_colnames = True) 
# Collecting the inferred rules inside the enemies dataframe 
rules_countries = association_rules(frq_items, metric ="lift",\
                                    min_threshold = 1) 
rules_countries = rules_countries.sort_values(['confidence', 'lift'],\
                                              ascending =[False, False])

## Mainly US <-> US

## Korea south -> north = 0
korea_south_north = enemies[enemies['actor.external.country.KR']==True]
korea_south_north = korea_south_north[korea_south_north['victim.country.KP']\
                                      ==True]

## Korea north to south = 18 incidents 
korea_north_south = enemies[enemies['actor.external.country.KP']==True]
korea_north_south = korea_north_south[korea_north_south['victim.country.KR']\
                                      ==True]

rules_countries.to_csv("usual_suspects.csv", index=False)