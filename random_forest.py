import pandas as pd
from sklearn import tree
# import graphviz

df = pd.read_csv("final.csv")

# # Industry
# X = df.drop("victim.industry.name", axis=1)
# X = pd.get_dummies(X)
#
# y = df["victim.industry.name"]
# y = pd.get_dummies(y)
#
# clf = tree.DecisionTreeClassifier(max_depth = 3)
# clf = clf.fit(X, y)
#
# tree.plot_tree(clf.fit(X, y))
# y.columns
# dot_data = tree.export_graphviz(clf, out_file=None,
#                                 feature_names=X.columns,
#                                 class_names=y.columns,
#                                 filled=True, rounded=True,
#                                 special_characters=True)
# graph = graphviz.Source(dot_data)
# graph.render("industry")
#
# # Breach or Not
#
# X = df.drop("attribute.confidentiality.data_disclosure", axis=1)
# X = pd.get_dummies(X)
#
# y = df["attribute.confidentiality.data_disclosure"] == "Yes"
#
# clf = tree.DecisionTreeClassifier(max_depth = 3)
# clf = clf.fit(X, y)
#
# tree.plot_tree(clf.fit(X, y))
#
# dot_data = tree.export_graphviz(clf, out_file=None,
#                                 feature_names=X.columns,
#                                 class_names=["Incident", "Breach"],
#                                 filled=True, rounded=True,
#                                 special_characters=True)
#
# graph = graphviz.Source(dot_data)
# graph.render("breach")
#
# # feature selection
# # forest trees algorith
from sklearn.ensemble import ExtraTreesClassifier
# from sklearn.feature_selection import SelectFromModel
#
# clf = ExtraTreesClassifier(n_estimators=10,max_depth=2)
# clf = clf.fit(X, y)
# clf.feature_importances_
# model = SelectFromModel(clf, prefit=True)
# cols = X.columns[model.get_support()]
# X_new = model.transform(X)
# X_new = pd.DataFrame(data=X_new, columns=cols)
#
# # kbest algorith
# from sklearn.feature_selection import SelectKBest
#
# kbest = SelectKBest(k=5)
# X_new2 = kbest.fit_transform(X, y)
# cols = X.columns[kbest.get_support()]
# X_new2 = pd.DataFrame(data=X_new2, columns=cols)
#

# Forest Clasification
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

# X = df.drop("victim.revenue.amount", axis=1)
X = df.drop("attribute.confidentiality.data.variety", axis=1)
X = df.drop("attribute.confidentiality.data_total", axis=1)
X = df.drop("attribute", axis=1)
X = df.drop("attribute.confidentiality.data_disclosure", axis=1)
X = pd.get_dummies(X)
Y = df["attribute.confidentiality.data_disclosure"] == "Yes"

x_train, x_test, y_train, y_test = train_test_split(X,Y,test_size=0.2,train_size=0.8, shuffle=True)

# clf = ExtraTreesClassifier(n_estimators=20,max_depth=4)
# clf.fit(x_train, y_train)
# # scores = cross_val_score(clf, x_train, y_train, cv=5)
# # mean score and the 95% confidence interval of scores
# # print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
# Accuracy = clf.score(x_test, y_test)
# print(Accuracy)
# y_pred = cross_val_predict(clf, x_train, y_train, cv=5)
#
# print(classification_report(y_true, y_pred, target_names=target_names))

from sklearn.svm import SVC
from sklearn.metrics import classification_report
clf = SVC(gamma='auto')
clf.fit(x_train, y_train)
Accuracy = clf.score(x_test, y_test)
print(Accuracy)
target_names = ['not Breach', 'Breach']
y_pred = clf.predict(x_test)

print(classification_report(y_test, y_pred, target_names=target_names))
#
# # SVM data_total
# X = df.drop("attribute.confidentiality.data_total", axis=1)
# X = pd.get_dummies(X)
# Y = df["attribute.confidentiality.data_total"] == "Yes"
#
# x_train, x_test, y_train, y_test = train_test_split(X,Y,test_size=0.2,train_size=0.8)
# clf = SVC(gamma='auto')
# clf.fit(x_train, y_train)
# Accuracy = clf.score(x_test, y_test)
# print(Accuracy)
# target_name = ['not Breach', 'Breach']
# y_pred = clf.predict(x_test)
# print(classification_report(y_test, y_pred, target_names=target_names))

y = df["attribute.confidentiality.data_disclosure"] == "Yes"

clf = tree.DecisionTreeClassifier(max_depth = 2)
clf = clf.fit(X, y)

tree.plot_tree(clf.fit(X, y))

dot_data = tree.export_graphviz(clf, out_file=None,
                                feature_names=X.columns,
                                class_names=["Not Breach", "Breach"],
                                filled=True, rounded=True,  
                                special_characters=True)

graph = graphviz.Source(dot_data)
graph.render("breach")

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel

clf = ExtraTreesClassifier(n_estimators=20,max_depth=2)
clf = clf.fit(X, y)
clf.feature_importances_  
model = SelectFromModel(clf, prefit=True)
cols = X.columns[model.get_support()]
X_new = model.transform(X)
X_new = pd.DataFrame(data=X_new, columns=cols)

importance = list(clf.feature_importances_)
colum = list(X.columns)
out = pd.DataFrame(list(zip(colum, importance)),columns =['Feature', 'Importance'])
from sklearn.feature_selection import SelectKBest


kbest = SelectKBest(k=5)
X_new2 = kbest.fit_transform(X, y)
cols = X.columns[kbest.get_support()]
X_new2 = pd.DataFrame(data=X_new2, columns=cols)
