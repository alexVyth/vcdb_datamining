from verispy import VERIS
import pandas as pd
import numpy as np

#get pandas from json
# data_dir = '../VCDB/data/json/validated'
# v = VERIS(json_dir=data_dir)
# veris_df = v.json_to_df(verbose=False)
# veris_df.to_csv("vcdb_json.csv")

df = pd.read_csv("ready.csv", index_col=0)

# delete all columns that contain Nans or all
# df = veris_df[veris_df["plus.analysis_status.Needs review"]==False]
# df = df[veris_df["plus.analysis_status.Ineligible"]==False]
# df = df[veris_df["plus.analysis_status.In-progress"]==False]

df = df.replace("Unknown", np.nan)
df = df.dropna(axis=1, thresh=6000)
df = df[['action', 'actor', 'asset.assets.variety', 'asset.total_amount',
       'asset.variety', 'attribute',
       'attribute.confidentiality.data_disclosure',
       'attribute.confidentiality.data_total', 'timeline.discovery.value',
       'timeline.compromise.value',
       'attribute.confidentiality.data.variety', 'impact.overall_amount',
       'pattern', 'timeline.incident.year',
       'victim.country', 'victim.industry', 'victim.industry.name', 'victim.region',
       'victim.revenue.amount']]
df.fillna("Unknown", inplace=True)
#df.to_csv("final.csv", index=False)

# cols = [c for c in df.columns if c != 'Unkwnown']
# df=df[cols]

# df = df.drop(columns=df.columns[(df == False).all()])
# df.to_csv("vcdb_clean.csv")



# =============================================================================
# Split dataset to boolean and numeric/str part
# =============================================================================
# numeric and str columns of dataframe
clean = pd.read_csv("vcdb_clean.csv", index_col=0)
non_bool = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64',
            'object']
non_bool_df = clean.select_dtypes(include=non_bool)

#for csv features
non_bool_df = non_bool_df.iloc[:,11:]
non_bool_df.to_csv("non_bool.csv", index=False)

# non_bool_df = non_bool_df[['incident_id', 'plus.analysis_status',
#                            'plus.timeline.notification.year',
#                            'timeline.incident.year',
#                            'victim.industry.name']]

# boolean part of dataframe
bool_df = clean.select_dtypes(include=bool)
bool_df.to_csv("bool.csv", index=False)